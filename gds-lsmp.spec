%define name 	gds-lsmp
%define version 2.19.2
%define release 1
%define daswg   /usr
%define prefix  %{daswg}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	gds-lsmp 2.19.2
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gds-base-devel >= 2.19.2
AutoReqProv: 	no
Prefix:		%prefix

%description
Global diagnostics software

%package  devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name}-headers = %{version}-%{release}

%description devel
GDS software development files.

%package   headers
Summary: 	 GDS header files.
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
#Requires: %{name}-lowlatency-headers = %{version}-%{release}

%description headers
GDS software header files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  \
    --prefix=%prefix \
    --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/gds
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*.so*

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/*
%{_libdir}/*.a

%files headers
%{_includedir}

%changelog
* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
